#pragma once
#include <istream>

#include <ios>
#include <vector>
#include <functional>
#include <Eigen/Core>
#include <string>
#include <iostream>
#include <fstream>
#include <ctime>
#include <cstdlib>

#include "neural_net.h"

//The class storing training data loaded from file.
//The class acts as an iterator, has overloaded operator ++,
//which causes move to next record (records are mixed so that they do not appear in the same order).
//Allows neural_net_trainer to get the vector with input data and with expected output.
class InputData
{
public:

    InputData(std::string file_name);

    const ai_library::Vector &getInput() const; //returns vector with input
    const ai_library::Vector &getOutput() const;  //returns vector with expected output
    InputData &operator++(); //move to next record

private:
    std::vector<std::pair<ai_library::Vector, ai_library::Vector>> data; //vector keeping records - pair: expected output and input
    std::vector<std::pair<ai_library::Vector, ai_library::Vector>>::iterator iterator; 
    int dataSize; //size input data
};

