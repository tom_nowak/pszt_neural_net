#ifndef SAMPLEFILE_H
#define SAMPLEFILE_H

#include "neural_net.h"
#include "ai_library_helpers.h"
#include <fstream>
#include <vector>

class SampleFile {
public:
    using value_type = std::pair<ai_library::Vector, ai_library::Vector>;

    SampleFile(std::string file_name) {
      std::ifstream file(file_name);
      ai_library::IstreamReader reader(file);
      reader.readFromNewLine(dataSize);
      value_type newPair;
      ai_library::Vector vRecord(dataSize);
      ai_library::Vector vData(1);

      while (reader.getLine()){

          reader.readFromCurrentLine(vData(0));
          for (size_t i = 0; i < dataSize; i++) {
              reader.readFromCurrentLine(vRecord(i));
          }
          newPair.first = vRecord;
          newPair.second = vData;
          data.push_back(newPair);
      }
    }

    size_t size() { return data.size(); }

    class iterator: public std::iterator<
        std::input_iterator_tag,   // iterator_category
        value_type, // value_type
        long,                      // difference_type
        value_type*, // pointer
        value_type& // reference
    >{
        SampleFile *obj;
        unsigned num = 0;
      public:
        explicit iterator(SampleFile* o, size_t n = 0) : obj(o), num(n) {}
        iterator& operator++() {++num; return *this;}
        iterator operator++(int) {iterator retval = *this; ++(*this); return retval;}
        bool operator==(iterator other) const {return num == other.num;}
        bool operator!=(iterator other) const {return !(*this == other);}
        reference operator*() const {return obj->data[num];}
    };
    iterator begin() {return iterator(this,0);}
    iterator end() {return iterator(this,data.size());}
private:
    std::vector<value_type> data;
    size_t dataSize;
};
#endif // SAMPLEFILE_H
