#pragma once
#include <istream>
#include <stdexcept>
#include <functional>
#include <Eigen/Core>

namespace ai_library
{
    using Matrix = Eigen::Matrix<float, Eigen::Dynamic, Eigen::Dynamic>;
    using Vector = Eigen::Matrix<float, 1, Eigen::Dynamic>;
    using ActivationFunctionType = std::function<float(float)>;

    namespace activation_functions
    {
        const ActivationFunctionType functions[] =
        {
            [](float x) { return x; }, // nothing - 0
            [](float x) { if(x > 0) return x; return 0.0f; }, // relu - 1
            [](float x) { return static_cast<float>(1.0 / ( 1.0 + std::exp(-x))); }, // sigmoid - 2
            [](float x) { return static_cast<float>(std::log(1 + std::exp(x))); } // softplus - 3
        };

        const ActivationFunctionType derivatives[] =
        {
            [](float x) { (void)x; return 1.0f; }, // nothing - 0
            [](float x) { if(x > 0) return 1.0f; return 0.0f; }, // relu - 1
            [](float x)
            {
                double tmp = std::exp(-x);
                double tmp1 = (1.0 + tmp)*(1.0 + tmp);
                return static_cast<float>(tmp / tmp1);
            }, // sigmoid - 2
            [](float x){ return static_cast<float>(1.0 / ( 1.0 + std::exp(-x))); } // softplus - 3
        };

        constexpr int size = sizeof(activation_functions::functions) / sizeof(ActivationFunctionType);
    }

    class IstreamReader
    {
    public:
        class Exception : public std::exception
        {
        public:
            Exception(unsigned line_number, const char *msg)
                : str("(at line " + std::to_string(line_number) + "): " + msg)
            {}

            virtual const char* what() const throw()
            {
                return str.c_str();
            }
        private:
            const std::string str;
        };

        IstreamReader(std::istream &is)
            : stream(is), lineNumber(0)
        {
        }

        bool getLine()
        {
            std::string string;
            std::getline(stream, string);
            if(!stream)
                return 0;
            ++lineNumber;
            iss.clear();
            iss.str(string);
            return !string.empty();
        }

        template <typename T>
        void readFromNewLine(T &t)
        {
            if(!getLine())
                throw Exception(lineNumber, "reading from new line failed");
            readFromCurrentLine(t);
        }

        template <typename T>
        void readFromCurrentLine(T &t)
        {
            iss >> t;
            if(!iss)
                throw Exception(lineNumber, "reading from current line failed");
        }

        template<typename T, typename ...Args>
        void readFromCurrentLine(T &t, Args &...args)
        {
            readFromCurrentLine(t);
            readFromCurrentLine(args...);
        }

    private:
        std::istream &stream;
        std::istringstream iss;
        unsigned lineNumber;
    };
}
