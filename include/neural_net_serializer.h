#pragma once
#include "neural_net.h"

// simple RAII class for NeuralNet serialization
class NeuralNetSerializer
{
public:
    NeuralNetSerializer(const char *file_name);
    ~NeuralNetSerializer();
    ai_library::NeuralNet &getNeuralNet();

private:
    // Saved to enable automatic serialization in destructor:
    const char *fileName;
    ai_library::NeuralNet nn;
};
