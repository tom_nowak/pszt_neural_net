Wydział Elektroniki i Technik Informacyjnych, Politechnika Warszawska  
 
## PSZT, Projekt z sieci neuronowych  
  
Łukasz Matuszewski, Tomasz Nowak, Maria Swianiewicz  
  
###  Treść zadania  
Stworzyć, wytrenować i przeprowadzić walidację sieci neuronowej, która dokona predykcji,   
czy mieszkańcy danego kraju są szczęśliwi, na podstawie danych   
https://www.kaggle.com/unsdsn/world-happiness.  

### Sposób kompilacji  
Wymagania: CMake, kompilator C++ zgodny ze standardem C++11, biblioteka Eigen.

Kompilacja (Linux):  
```
mkdir build 
cd build
cmake .. 
make  
```

Na innych systemach - CMake to przenośne narzędzie, utworzy projekt zgodny z wymaganiami danego środowiska (np. projekt VisualStudio na Windowsie).   