#include "neural_net_trainer.h"

using namespace ai_library;

NeuralNetTrainer::NeuralNetTrainer(NeuralNet &nn)
    : neuralNet(nn), deltas(neuralNet.layers.size())
{
}

float NeuralNetTrainer::getErrorValue(const ai_library::Vector &output, const ai_library::Vector &expected_output)
{
    return (expected_output - output).squaredNorm()/2.0f;
}

void NeuralNetTrainer::setDeltaInOutputLayer(const ai_library::Vector &expected_output)
{
    // This function must be called after forward propagation because it uses values saved in NN layers.
    ai_library::Vector &delta = deltas[deltas.size() - 1];
    NeuralNet::Layer &layer = neuralNet.layers[deltas.size() - 1];
    // Error function: 1/2 (expected_output - layer.output)^2
    // Its derivative over weights:
    // (layer.output - expected_output) * ((delta layer.output) / (delta weights))
    // (delta layer.output) / (delta weights) = ((delta layer.output) / (delta layer.input)) * ((delta layer.input) / (delta weights))
    // (delta layer.output) / (delta layer.input) = activationFunctionDerivative(layer.input)
    // (delta layer.input) / (delta weights) depends on values in previous layer, it will be calculated later
    delta.noalias() = neuralNet.getOutput() - expected_output;
    // element-wise vector (or matrix) multiplication (Hadamard product) with input derivative:
    delta = delta.cwiseProduct(layer.input.unaryExpr(layer.activationFunctionDerivative()));
}

void NeuralNetTrainer::propagateBackward()
{
    // This function must be called after forward propagation and setDeltaInOutputLayer because it uses values saved in NN and delta layers.
    for(int layer_number = deltas.size() - 2; layer_number >= 0; --layer_number)
    {
        // references for readability:
        ai_library::Vector &delta = deltas[layer_number];
        ai_library::Vector &previous_delta = deltas[layer_number + 1];
        NeuralNet::Layer &layer = neuralNet.layers[layer_number];
        NeuralNet::Layer &previous_layer = neuralNet.layers[layer_number + 1];

        // Each delta element (corresponding to one neuron in layer) "collects" delta from all
        // neurons it is connected with in the next layer:
        delta.noalias() = previous_delta * previous_layer.weights.transpose();
        // Apply element-wise multiplication by activationFunctionDerivative(layer.input)
        delta = delta.cwiseProduct(layer.input.unaryExpr(layer.activationFunctionDerivative()));
    }
}

void NeuralNetTrainer::updateWeights(const ai_library::Vector &input)
{
    // In 0 layer - references for readability:
    const ai_library::Vector &delta = deltas[0];
    NeuralNet::Layer &layer = neuralNet.layers[0];

    // Apply simple gradient descent:
    layer.weights -= neuralNet.weightsLearningRate * input.transpose() * delta; // input.transpose() * delta forms a matrix the same size as weights
    layer.bias -= neuralNet.biasLearningRate * delta; // bias is like virtual weights row - from const 1 neuron in input layer

    for(unsigned layer_number = 1; layer_number < deltas.size(); ++layer_number)
    {
        const ai_library::Vector &delta = deltas[layer_number];
        NeuralNet::Layer &layer = neuralNet.layers[layer_number];
        const ai_library::Vector &previous_layer_output = neuralNet.layers[layer_number - 1].output;
        layer.weights -= neuralNet.weightsLearningRate * previous_layer_output.transpose() * delta;
        layer.bias -= neuralNet.biasLearningRate * delta; // bias is like virtual weights row - from const 1 neuron in prevoius layer
    }
}

void NeuralNetTrainer::updateLearningRate()
{

}
