#include "input_data.h"

InputData::InputData(std::string file_name) {
    std::ifstream file(file_name);
    ai_library::IstreamReader reader(file);
    reader.readFromNewLine(dataSize);
    std::pair<ai_library::Vector, ai_library::Vector> newPair;
    ai_library::Vector vRecord(dataSize);
    ai_library::Vector vData(1);

    while (reader.getLine()){

        reader.readFromCurrentLine(vData(0));
        for (int i = 0; i < dataSize; i++) {
            reader.readFromCurrentLine(vRecord(i));
        }
        newPair.first = vRecord;
        newPair.second = vData;
        data.push_back(newPair);
    }
    std::srand(unsigned(std::time(0)));
    std::random_shuffle(data.begin(), data.end());

    iterator = data.begin();
}

const ai_library::Vector &InputData::getInput() const{
    return iterator->first;
}

const ai_library::Vector &InputData::getOutput() const{
    return iterator->second;
}

InputData &InputData::operator++() {
    iterator++;
    if (iterator == data.end()) {
        iterator = data.begin();
        std::random_shuffle(data.begin(), data.end());
    }
    return *this;
}
