#include <iostream>
#include <iomanip>
#include <cstring>
#include <string>
#include <thread>
#include "input_data.h"
#include "output_data.h"
#include "neural_net.h"
#include "neural_net_trainer.h"
#include "neural_net_serializer.h"
#include "sample_file.h"

namespace
{
    void help(const char *program_name)
    {
        std::cout << "\nUsage: " << program_name << " [command] [nn_file] [data file] [[test data file]]\n\n"
                     "Available commands:\n\n"
                     "- train-silent [nn_file] [training_file]\n    saves settings file with trained network parameters\n"
                     "- train-verbose [nn_file] [training_file] [test_file]\n    saves settings file (with trained network parameters),"
                     "\n    prints training report to standard output.\n"
                     "\n  additional options (none/some/all): \n"
                     "\n  i[iterations] - iteration limit \n"
                     "\n  s[sub-iterations] - iteration in each validation turn \n"
                     "\n  e[sub-iterations] - target error level \n"
                     "- predict [nn_file] [test_data_file]\n    predicts values for test data based on nn_file\n"
                     "- validate [nn_file] [test_data_file]\n    prints neural network output and corresponding target value\n    from the training set\n\n"
                     "Expected data format:\n"
                     "- nn_file - AI_library::Neural net settings file\n"
                     "- data file/test data file - InputData file\n";
        std::exit(1);
    }
    // functions performing each command listed in the help message above
    void trainSilent(const char *neural_net_settings, const char *input_file);
    void trainVerbose(const char *neural_net_settings, const char *input_train_file, const char *input_test_file,
                      size_t max_iter = 10000, size_t sub_iter = 10000, float targetErr = 0.00000001);
    void predict(const char *neural_net_settings, const char *input_test_file);
    void validate(const char *neural_net_settings, const char *input_test_file);

    // calculate error on network output after processing data
    // used in trainVerbose(...)
    float getAverageError(SampleFile &data, ai_library::NeuralNet &nn);
}

int main(int argc, char **argv)
{
    try
    {
        if (argc < 4)
            help(argv[0]);

        if (std::strcmp(argv[1], "train-silent") == 0) {
            if(argc != 4)
                help(argv[0]);
            trainSilent(argv[2], argv[3]);
        } else if (std::strcmp(argv[1], "train-verbose") == 0) {
            if(argc < 5)
                help(argv[0]);
            float target_err = 0.0000001;
            size_t sub_iter = 10000;
            size_t max_iter = 1000000;

            // update train-verbose specific parameters if specified
            for(int i = 5; i < argc; ++i) {
                if(strlen(argv[i]) < 2)
                  help(argv[0]);
                std::string s(&argv[i][1]);
                switch(argv[i][0]) {
                  case 'i':
                    max_iter = std::stoi(s);
                    std::cerr << " set max_iter=" << max_iter << std::endl;
                    break;
                  case 's':
                    sub_iter = std::stoi(s);
                    std::cerr << " set sub-iter=" << sub_iter << std::endl;
                    break;
                  case 'e':
                    target_err = std::stof(s);
                    std::cerr << " set target_err=" << target_err << std::endl;
                    break;
                  default:
                    help(argv[0]);
               }
            }

            trainVerbose(argv[2], argv[3], argv[4], max_iter, sub_iter, target_err);
        } else if (std::strcmp(argv[1], "predict") == 0) {
            if(argc != 4)
                help(argv[0]);
            predict(argv[2], argv[3]);
        } else if (std::strcmp(argv[1], "validate") == 0) {
            if(argc != 4)
                help(argv[0]);
            validate(argv[2], argv[3]);
        } else
            help(argv[0]);
        return 0;
    }
    catch(const std::exception &ex)
    {
        std::cerr << "Exception caught: " << ex.what() << "\n";
        return -1;
    }
    catch(const char *ex)
    {
        std::cerr << "Exception caught: " << ex << "\n";
        return -1;
    }
    catch(...)
    {
        std::cerr << "Unknown exception caught\n";
        return -1;
    }
}

namespace {
  //  Silent training without any additional operations.
  //  The training is controlled by keyboard input
  //     q - stop
  //     p - show iteration number

  void trainSilent(const char *neural_net_settings, const char *input_train_file) {
      struct Functor // will be changed to more sophisticated
      {
          Functor(bool i)
                  :  iter(0), stop(i), peek(0) {}

          bool operator()() {
              ++iter;
              if(stop) {
                std::cout << "Finished training after " << iter << " iterations." << std::endl;
              } else if(peek) {
                 std::cout << "Iteration: " << iter << std::endl;
                 peek = false;
              }
              return stop;
          }
          unsigned long iter;
          bool stop, peek;
      } functor(false);

      // read the given nn configuration file
      NeuralNetSerializer serializer(neural_net_settings);
      ai_library::NeuralNet &nn = serializer.getNeuralNet();
      InputData data(input_train_file);
      ai_library::NeuralNetTrainer trainer(nn);

      std::cout << "Running silent training. Type 'q' to finish, 'p' to check iteration number." << std::endl << std::endl;
      // the training is interrupted when cin reads 'q'
      std::thread train([&] { trainer.train(data, functor); });
      std::thread supervisor([&] {
          char c = 0;
          while (c != 'q') {
            c = std::cin.get();
            if(c == 'p') functor.peek = true;
          };
          functor.stop = true;
      });
      train.join();
      supervisor.join();
  }

  //  train-verbose command runs a series of trainings based on input_training_file
  //  followed by calculating errors based on input_test_file

  void trainVerbose(const char *neural_net_settings, const char *input_train_file, const char *input_test_file,
                    size_t max_iter, size_t sub_iter, float targetErr) {
      struct Functor // will be changed to more sophisticated
      {
          Functor(size_t i)
                  : iter(i) {}

          bool operator()() {
              --iter;
              if(iter == 0) return true;
              return false;
          }
          unsigned long iter;
      } fun(10000);

      NeuralNetSerializer serializer(neural_net_settings);
      ai_library::NeuralNet &nn = serializer.getNeuralNet();
      InputData data(input_train_file);
      SampleFile test_data(input_test_file);
      ai_library::NeuralNetTrainer trainer(nn);
      std::cerr << "Running " << max_iter * sub_iter << " iterations of training. Type 'q' to finish." << std::endl << std::endl;
      std::cout << std::setw(20) << "number of iterations" << std::setw(20) << "average error" << std::endl;

      bool stop = false;
      std::thread supervisor([&] {
          while (std::cin.get() != 'q');
          stop = true;
      });
      for(size_t i=0; i<max_iter; i++) {
          fun.iter = sub_iter;
          trainer.train(data, fun);
          float t = getAverageError(test_data, nn);
          std::cout << std::setw(20) << i*sub_iter << std::setw(20) << t << std::endl;
          if(t <= targetErr) {
              std::cerr << "Reached target error level " << targetErr << " after " << (i * sub_iter) << " iterations.\n";
              break;
          }
          if(stop)
              break;
      }
      if(!stop)
          std::cerr << "Training finished. Type q to exit.";
      supervisor.join();
  }

  // calculate error on network output after processing data
  float getAverageError(SampleFile& file, ai_library::NeuralNet &nn)
  {
      float sum = 0;
      for(auto& v : file)
      {
          nn.propagateForward(v.first);
          sum += (nn.getOutput() - v.second).squaredNorm();
      }
      return sum;
  }

  //  Propagate forward data from a file and display output for each row.
  //  Requires the same file format as InputData, just ignores the result fields.

  void predict(const char *neural_net_settings, const char *input_test_file) {
      NeuralNetSerializer serializer(neural_net_settings);
      ai_library::NeuralNet &nn = serializer.getNeuralNet();

      SampleFile sf(input_test_file);

      std::cout << "Predicted the values for data set " << input_test_file << ":\n";
      for(auto& v : sf) {
          nn.propagateForward(v.first);
          std::cout << nn.getOutput() << std::endl;
      }
  }

  //  Propagate forward data from a file and display output for each row
  //  compared to target values given in the file.
  //  Requires the same file format as InputData, just ignores the result fields.

  void validate(const char *neural_net_settings, const char *input_test_file) {
      NeuralNetSerializer serializer(neural_net_settings);
      ai_library::NeuralNet &nn = serializer.getNeuralNet();

      SampleFile sf(input_test_file);

      std::cout << std::setw(16) << "NN out\t\ttarget value" << std::endl;
      for(auto& v : sf) {
          nn.propagateForward(v.first);
          std::cout << " " << nn.getOutput() << "\t" << v.second << std::endl;
      }
  }
}
