#include "neural_net_serializer.h"
#include <fstream>
#include <iostream>

NeuralNetSerializer::NeuralNetSerializer(const char *file_name)
    : fileName(file_name)
{
    std::ifstream file(fileName);
    if(!file)
        throw std::invalid_argument("failed to open NeuralNet config file");
    nn.initialize(file);
}

NeuralNetSerializer::~NeuralNetSerializer()
{
    std::ofstream file(fileName);
    if(!file)
    {
        std::cerr << "Error opening NeuralNet serialization file - writing to cout to prevent data loss\n";
        nn.serialize(std::cout);
    }
    nn.serialize(file);
}

ai_library::NeuralNet &NeuralNetSerializer::getNeuralNet()
{
    return nn;
}
